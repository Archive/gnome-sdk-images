This repository is deprecated.

Branches master and 3.30 of org.gnome.Sdk are now maintained over at
[gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta), and all
future branches will be maintained there.